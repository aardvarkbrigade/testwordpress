<?php
/* Register Custom SO Page Builder widgets */
add_filter('siteorigin_widgets_widget_folders', function ($folders) {
    $folders[] = __DIR__ . '/../siteorigin-widgets/';
    return $folders;
});