# How to install
1. Ensure you are using v8 of node or greater

    ```nvm use v8```

2. Install the required node packages

    ```npm install```

3. [Composer](http://getcomposer.org) is used for installing the WP plugins that are required to run the theme

    ```composer install```

4. Make a copy of `.env.example`, call it `.env`, and set the `HTTP_PORT` and `MYSQL_PORT` to ports you'd like to expose on your computer

5. Run `docker-compose up`

6. Visit `http://127.0.0.1:HTTP_PORT`, and the site should be up and running

7. Run `npm run start` to begin compiling your JS + SCSS

# Deploying
When ready to deploy, run `npm run build` for a production ready release of the `build/` folder